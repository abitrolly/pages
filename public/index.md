* All content should be placed into `public/` directory
* Then a minimal `.gitlab-ci.yml` file should be created to signal to GitLab that you have pages

```yaml
pages:
  script:
    - echo "Magic job that uploads 'public/' to https://abitrolly.gitlab.io/pages/"
```
